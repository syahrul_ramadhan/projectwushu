<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Galery</title>
    <link rel="stylesheet" href="{{ asset('/wushu-project') }}/css/style1.css">
    <link rel="stylesheet" href="{{ asset('/wushu-project') }}/css/style.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.9.1/font/bootstrap-icons.css">
</head>

<body>

    <x-navbar/>

    <header class="gallery">
        <div class="d-flex justify-content-center align-items-center h-100 text-light fw-bold">
            <h1 class="news text-upppercase">Galery</h1>
        </div>
    </header>

    <section id="gallery">
        <div class="container mt-5">
            <div class="p-4">

                <div class="row">
                    @foreach ($galeries as $item)
                    @php
                        $images = asset('/uploads/images/' . $item->media);
                        if (is_null($item->media)) {
                            $images = asset('/uploads/images/default.png');
                        }
                    @endphp
                    <div class="col-md-4">
                        <div class="card mb-5 shadow rounded border" style="max-width: 700px;">
                            <img src="{{$images}}"
                                class="card-img-top" alt="{{$item->title ?? 'media image'}}">
                            <div class="card-body text-center p-4">
                                <a class="card-title mb-2 text-uppercase h5 text-decoration-none text-dark" href="{{ route('wushu.galeries-detail', ['galeryId' => $item->id]) }}">
                                    {{$item->title ?? 'lorem ipsum'}}
                                </a>
                                <p class="card-text">
                                    {{$item->description ?? ''}}
                                </p>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>

                {!! $galeries->links() !!}

            </div>
        </div>
    </section>

    <x-footer/>

    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js"
        integrity="sha384-oBqDVmMz9ATKxIep9tiCxS/Z9fNfEXiDAYTujMAeBAsjFuCZSmKbSSUnQlmh/jp3" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.min.js"
        integrity="sha384-IDwe1+LCz02ROU9k972gdyvl+AESN10+x7tBKgc9I5HFtuNz0wWnPclzo6p9vxnk" crossorigin="anonymous">
    </script>
</body>

</html>
